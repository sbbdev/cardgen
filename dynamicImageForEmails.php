<?php

// Define our variables temportarly - move to DB call after testing
$rewardID = htmlspecialchars($_GET['reward']);
$name = htmlspecialchars($_GET['n']);
$denomination = htmlspecialchars($_GET['d']);
$pin = htmlspecialchars($_GET['p']);
$logo = htmlspecialchars($_GET['l']);
$noBG = htmlspecialchars(($_GET['nobg'] == 'true' ? true : false));
$rewardID = htmlspecialchars($_GET['reward']);

// Settings
$noLogo = 'assets/images/emailNoLogo.png';
$logoHeight = 100;
$logoWidth = 400;

// Data
$data = [
    'name' => [
        'x' => ($noBG ? 150 : 400),
        'y' => ($noBG ? 700 : 920),
        'size' => 40,
        'angle' => 0,
        'content' => $name,
    ],
    'pin' => [
        'x' => ($noBG ? 130 : 440),
        'y' => ($noBG ? 475 : 700),
        'size' => 75,
        'angle' => 0,
        'content' => implode('  ', str_split($pin)),
    ],
    'denomination' => [
        'x' => denominationPosition($denomination, $noBG),
        'y' => ($noBG ? 150 : 375),
        'size' => 70,
        'angle' => 0,
        'content' => $denomination,
    ],
    'defaultText' => [
        'x' => ($noBG ? 150 : 400),
        'y' => ($noBG ? 780 : 980),
        'size' => 30,
        'angle' => 0,
        'content' => 'It Doesn\'t Cost - It PAY$!',
    ],
    'logo' => [
        'x' => ($noBG ? 875 : 1200),
        'y' => ($noBG ? 675 : 880),
    ],
];

// Are we using the template with or without a background?
if($noBG){
    $style = 'assets/images/templateNoBG.png';
}else{
    $style = 'assets/images/template.png';
}


// Did we pass a logo?
if ($logo) {
    $src = imagecreatefrompng($logo);
}else{
    $src= imagecreatefrompng($noLogo);
}

// Transparent sponsor logo
imagealphablending($src, false);
imagesavealpha($src, true);

// Define our source image (the background)
$destination = imagecreatefrompng($style);

// Colors
$textColor = imagecolorallocate($destination, 255, 255, 255);
$regularFont = 'assets/fonts/Bungee-Regular.otf';

// Transparent background
imagealphablending($destination, true);
imagesavealpha($destination, true);

// Name
imagettftext($destination, $data['name']['size'], $data['name']['angle'], $data['name']['x'], $data['name']['y'], $textColor, $regularFont, $data['name']['content']);
// Pin
imagettftext($destination, $data['pin']['size'], $data['pin']['angle'], $data['pin']['x'], $data['pin']['y'], $textColor, $regularFont, $data['pin']['content']);
// Denomination
imagettftext($destination, $data['denomination']['size'], $data['denomination']['angle'], $data['denomination']['x'], $data['denomination']['y'], $textColor, $regularFont, '$' . $data['denomination']['content']);
// Default Text
imagettftext($destination, $data['defaultText']['size'], $data['defaultText']['angle'], $data['defaultText']['x'], $data['defaultText']['y'], $textColor, $regularFont, $data['defaultText']['content']);

// Merge the logo and template together
imagecopy($destination, $src, $data['logo']['x'], $data['logo']['y'], 0, 0, $logoWidth, $logoHeight);


// Create our header to flush the image to browser
//header("Content-type: image/png");
header("Content-type: image/gif");
header("Cache-Control: no-store, no-cache");

/**
 * Un-comment to ask to save file
 * header('Content-Disposition: attachment; filename="DiningCard.png"');
 */

// Render image
//imagepng($destination); // Works
imagegif($destination);

// Cleanup
imagedestroy($destination);


/**
 * Depending on the number of of numbers in the denomination,
 * pass x coordinates to help keep it in position
 */
function denominationPosition($denomination, $noBG)
{
    switch (strlen($denomination)) {
        case 1:
            return ($noBG ? 1150 : 950);
            break;
        case 2:
            return ($noBG ? 1100: 1400);
            break;
        case 3:
            return ($noBG ? 1050 : 1350);
            break;
        case 4:
            return ($noBG ? 950 : 1290);
            break;
    }

}
