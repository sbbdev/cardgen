
$(document).ready(function() {

    // Default text for card
    var defaultText = 'It Doesn’t Cost - It PAY$!';
    $('.defaultText').empty().append(defaultText);
    $('#defaultText').val(defaultText);

    // On name change
    $('#name').on('input', function(e) {
        $('.name').empty().append(e.target.value);
    });

    // On pin change
    $('#pin').on('input', function(e) {
        $('.pin').empty().append(e.target.value);
    });

    // On denomination change
    $('#denomination').change(function(e) {
        $('.denomination').empty().append('$' + e.target.value);
        // Adjust position
        var position = denominationPosition(e.target.value);
        $('.denomination').css('margin-left', position);
    });

    // On "Browse"
    $(document).on('click', '.browse', function() {
        var file = $(this).parent().parent().parent().find('.file');
        file.trigger('click');
    });

    // Upon selecting a file, trigger the underlying input
    $(document).on('change', '.file', function(ev) {

        $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));

        var f = ev.target.files[0];
        var fr = new FileReader();

        fr.onload = function(ev2) {
            // Append the logo to the DOM from the local machine
            $('#logo').attr('src', ev2.target.result);
        };

        fr.readAsDataURL(f);

    });

    // Save our image
    $('#generate').click(function(e) {
        e.preventDefault();
        validate();
    });

    /**
     * Depending on the denomination, adjust position accordingly
     */
    function denominationPosition(denomination){
        switch(denomination.length){
            case 1:
                return 100;
            break;
            case 2:
                return 760;
            break;
            case 3:
                return 725;
            break;
            case 4:
                return 700;
            break;
        }
    }


    /**
     * Are all fields filled out?
     */
    function validate() {

        var errors = Array();

        if (!$('#denomination').val()) {
            errors.push('Please select a denomination.');
        }
        if (!$('#name').val()) {
            errors.push('Please enter a name.');
        }
        if (!$('#pin').val()) {
            errors.push('Please enter a pin.');
        }

        // Do we have errors?
        if (errors.length) {
            $('#errors').empty().append('<div class="alert alert-warning">' + errors.join("<br />") + '</div>');
        } else {
            $('#cardForm').submit();
        }

    }

})
