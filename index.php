<?php

if (isset($_POST['action']) && $_POST['action'] == 'generate') {

    // Define our posted vars
    $denomination = html_entity_decode(strtoupper($_POST['denomination']));
    $name = html_entity_decode($_POST['name']);
    $pin = html_entity_decode(implode(' ', str_split($_POST['pin'])));
    $defaultText = html_entity_decode($_POST['defaultText']);
    $style = 'assets/images/template.png';
    $demoLogo = 'assets/images/noLogo.png';

    // Did we upload a logo?
    if ($_FILES["logo"]["name"]) {

         // Logo Uploads
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["logo"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        $logoError = '';
        $allowedTypes = ['png'];

        $check = getimagesize($_FILES["logo"]["tmp_name"]);

        // Is this an image?
        if ($check == false) {
            $logoError = "File is not an image";
            $uploadOk = 0;
            header('Location: index.php?error=' . $logoError);
        }

        // Check file size
        if ($_FILES["logo"]["size"] > 500000) {
            $logoError = "Sorry, your file is too large.";
            $uploadOk = 0;
            header('Location: index.php?error=' . $logoError);
        }

        // Allow certain file formats
        if (!in_array($imageFileType, $allowedTypes)) {
            $logoError = "Invalid image file type.";
            $uploadOk = 0;
            header('Location: index.php?error=' . $logoError);
        }

        if ($uploadOk == 1) {
            if (move_uploaded_file($_FILES["logo"]["tmp_name"], $target_file)) {
                //  Good
            } else {
                $logoError = 'Sorry, there was an error uploading your file.';
                header('Location: index.php?error=' . $logoError);
            }
        }

    }

    // If we have all of the post data
    if ($denomination && $name && $pin && $defaultText) {

        // Define our source image
        $destination = imagecreatefrompng($style);

        if($_FILES["logo"]["name"]){
            $src = imagecreatefrompng($target_file);
        }else{
            $src = imagecreatefrompng($demoLogo);
        }

        // Preserve transparency
        imagealphablending($destination, true);
        imagesavealpha($destination, true);

        // Colors
        $textColor = imagecolorallocate($destination, 255, 255, 255);
        $font = 'assets/fonts/Bungee-Regular.otf';

        // Data
        $data = [
            'name' => [
                'x' => 400,
                'y' => 920,
                'size' => 40,
                'angle' => 0,
                'content' => $name,
            ],
            'pin' => [
                'x' => 475,
                'y' => 700,
                'size' => 75,
                'angle' => 0,
                'content' => implode(' ', str_split($pin)),
            ],
            'denomination' => [
                'x' => denominationPosition($denomination),
                'y' => 375,
                'size' => 70,
                'angle' => 0,
                'content' => $denomination,
            ],
            'defaultText' => [
                'x' => 400,
                'y' => 980,
                'size' => 30,
                'angle' => 0,
                'content' => 'It Doesn\'t Cost - It PAY$!',
            ],
            'logo' => [
                'x' => 1200,
                'y' => 880,
            ],
        ];

        // Name
        imagettftext($destination, $data['name']['size'], $data['name']['angle'], $data['name']['x'], $data['name']['y'], $textColor, $font, $data['name']['content']);
        // Pin
        imagettftext($destination, $data['pin']['size'], $data['pin']['angle'], $data['pin']['x'], $data['pin']['y'], $textColor, $font, $data['pin']['content']);
        // Denomination
        imagettftext($destination, $data['denomination']['size'], $data['denomination']['angle'], $data['denomination']['x'], $data['denomination']['y'], $textColor, $font, '$' . $data['denomination']['content']);
        // Default Text
        imagettftext($destination, $data['defaultText']['size'], $data['defaultText']['angle'], $data['defaultText']['x'], $data['defaultText']['y'], $textColor, $font, $data['defaultText']['content']);
    
        // Merge the logo and template together
        imagecopymerge_alpha($destination, ($_FILES["logo"]["name"] ? $_FILES["logo"]["name"] : $src), $data['logo']['x'], $data['logo']['y'], 0, 0, 400, 100, 100);
        

        // Create our header to flush the image to browser
        header("Content-type: image/png");
        header("Cache-Control: no-store, no-cache");
        header('Content-Disposition: attachment; filename="$'.$denomination.'DiningCard.png"');
        
        // Render image
        imagepng($destination);

        // Cleanup
        imagedestroy($destination);
        if($_FILES["logo"]["name"]){
            unlink($target_file);
        }
    }
}

/**
 * Depending on the number of of numbers in the denomination,
 * pass x coordinates to help keep it in position
 */
function denominationPosition($denomination)
{
    switch (strlen($denomination)) {
        case 1:
            return 1200;
            break;
        case 2:
            return 1400;
            break;
        case 3:
            return 1350;
            break;
        case 4:
            return 1290;
            break;
        default:
            return 1400;
            break;
    }

}

/**
 * Fix alpha channel to allow for transparent logos
 */
function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct)
{
    // creating a cut resource
    $cut = imagecreatetruecolor($src_w, $src_h);

    // copying relevant section from background to the cut resource
    imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h);

    // copying relevant section from watermark to the cut resource
    imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h);

    // insert cut resource to destination image
    imagecopymerge($dst_im, $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct);
}
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="Card Generator">
      <meta name="author" content="Carl Hussey">
      <title>Dining Card Generator</title>
      
      <!-- CSS -->
      <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="assets/css/style.css" rel="stylesheet">

      <!-- Javascript -->
      <script src="assets/javascript/jquery/jquery.min.js"></script>
      <script src="https://use.fontawesome.com/cb34b21666.js"></script>

      <!-- Card Generator -->
      <link href="assets/css/card.css" rel="stylesheet">
      <script src="assets/javascript/card.js"></script>
      
   </head>
   <body>
      <!-- Navigation -->
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
         <div class="container">
            <a class="navbar-brand" href="index.php">Card Generator</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
               aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
               <ul class="navbar-nav ml-auto">
                  <li class="nav-item active">
                     <a class="nav-link" href="index.php">Home
                     <span class="sr-only">(current)</span>
                     </a>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
      <!-- Page Content -->
      <div class="container">
         <!-- Jumbotron Header -->
         <div class="d-block d-sm-none d-md-none d-lg-block">
            <br /><br />
         </div>
         <header class="jumbotron my-4 cardParent cardImage d-none d-lg-block">
            <div class="name">Sam A. Smith</div>
            <div class="denomination">$75</div>
            <div class="pin">ABCDE123</div>
            <div class="defaultText"></div>
            <div class="logo">
                <img id="logo" width="80%" height="80%" src="assets/images/sample.png"/>
            </div>
         </header>
         <div class="errors" id="errors"></div>
         <form name="cardForm" id="cardForm" action="index.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
               <label>Denomination</label>
               <select name="denomination" id="denomination" class="form-control">
                  <option value="">Select a Denomination</option>
                  <option value="25">$25</option>
                  <option value="50">$50</option>
                  <option value="75" selected="selected">$75</option>
                  <option value="100">$100</option>
               </select>
            </div>
            <div class="form-group">
               <label>Name</label>
               <input type="text" class="form-control" id="name" name="name" placeholder="Enter a Name" value="Sam A. Smith" maxlength="22">
            </div>
            <div class="form-group">
               <label>Pin Number</label>
               <input type="text" class="form-control" id="pin" name="pin" placeholder="Pin Number" value="ABCDE123" maxlength="8">
            </div>
            <div class="form-group">
               <label>Company Logo <small>(optional)</small></label>
               <input type="file" name="logo" class="file">
               
               <div class="input-group col-xs-12">
                  <span class="input-group-addon"><i class="fa fa-file-image-o"></i></span>
                  <input type="text" class="form-control input-lg" disabled placeholder="Upload Image">
                  <span class="input-group-btn">
                  <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-search"></i> Browse</button>
                  </span>
               </div>
               <p><small>Image must be 400w x 100h in .png format</small></p>
            </div>
            <input type="hidden" name="action" value="generate">
            <input type="hidden" name="defaultText" id="defaultText" value="">
            <br />
            <button type="submit" class="btn btn-primary" name="generate" id="generate"><i class="fa fa-refresh"></i> Generate Card</button>
         </form>
      </div>
      <!-- Bootstrap core JavaScript -->
      <script src="assets/bootstrap/js/bootstrap.bundle.min.js"></script>
   </body>
</html>